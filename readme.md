## Prueba para Tu aplicación

## Introducción

Para esta prueba modifiqué un poco algunos archivos del framework para organizar el codigo de manera un poco diferente a la habitual, con el fin de organizar todo por modulos. Si bien esto no era realmente necesario me gusta trabajar de este modo ya que al ir creciendo la aplicación en tamaño y complejidad, esta estructura facilita el mantenimiento y el remplazo o la introduccion de funcionalidades.

Se modificó en archivo `composer.json` para agragar un nuevo nombre de espacio que le indique al framework donde buscar las clases de los nuevos modulos.
```
"autoload": {
    ...
    "psr-4": {
        "App\\": "app/",
        "Api\\": "api/"  <-- Nuevo nombre de espacio
    }
},
```
**Nota**: si al probar la aplicación arroja un error diciendo que no existen los controladores se debe ejecutar en la terminal el comando `composer dump-autoload` para cargar los nuevos nombres de espacio.


Se modificó la clase `App\Providers\AppServiceProvider` agregando dos nuevos métodos para registrar las rutas y las migraciones de los nuevos modulos y ajecutandolos en el método boot de la clase (también se puede crear un service provider especificamente para esto).

También se modificó la opción `providers.users.model` del archivo de configuración `auth.php` para que el ORM utilice el modelo de usuario definido en el modulo de usuarios (por defecto usa el modelo `App\User` que viene incluido con el framework).

Por último, se modificó el archivo de configuracion `database.php` porque al hacer pruebas con postman no reconocía las variables de entorno del archivo `.env` relativas a la conexión con la base de datos. Debe tenerse esto en cuenta al momento de configurar la base de datos con la que se vaya a probar.

## Estuctura de los modulos

Cada modulo tiene la siguiente estructura.

```
namespace/
└── Module/
    ├── Controllers/
    ├── Database/
    │   └── Migrations/
    ├── Models/
    ├── Repositories/  --> Clases donde se ubica todo lo relacionado con acceso a datos
    ├── Requests/
    ├── Resources/
    └── routes.php
```

## Endponits

El API sirve dos recursos segun lo requerido:

- **Informacion básica de usuarios**

| Method | Endpoint  | Descripción  | Campos |
|:-------|:----------|:-------------|:-------|
| get    | api/users | Lista todos los usuarios. | |
| post   | api/user  | Crea un nuevo usuario.| requerido[name,lastname,phone,email,address,password, password_confirmation]|
| get    | api/user  | Muestra la informacion de un usuario.| requerido[id]|
| put    | api/user  | Actualiza la informacion de un usuario.| requerido(id,name,lastname,phone,email,address), opcional[password, password_confirmation] |
| delete | api/user  | Elimina un usuario.| requerido[id]|

- **Datos adicionales**

| Method | Endpoint            | Descripción                                           | Campos                |
|:-------|:--------------------|:------------------------------------------------------|:----------------------|
| get    | api/additionals     | Lista informacion adicional                           |                       |
| post   | api/additional      | Crea un nueva informacion adicional.                  | requerido[additional] |
| get    | api/additional/{id} | Muestra la informacion de una informacion adicional   |                       |
| put    | api/additional/{id} | Actualiza la informacion de una informacion adicional |                       |
| delete | api/additional/{id} | Elimina una informacion adicional                     |                       |

**Nota**: en cada petición deben enviarse el header 'Accept': 'application/json'

## Ejemplos

Ejemplos de peticiones con curl.

- **Usuarios**

**List:**
curl -H 'Accept: application/json' http://localhost:8000/api/users

**Create:**
curl -X POST -H 'Content-type: application/json' -H 'Accept: application/json' -d '{"name":"ricardo","lastname":"feria","phone":"3105179674","email":"rferia91@gmail.com","address":"Cra. 21 #71-60","password":"123456","password_confirmation":"123456"}' http://localhost:8000/api/user

**Read:**
curl -H 'Accept: application/json' http://localhost:8000/api/user?id=1

**Update:**
curl -X PUT -H 'Content-type: application/json' -H 'Accept: application/json' -d '{"id":"1","name":"Ricardo","lastname":"Feria","phone":"3105179674","email":"rferia91@gmail.com","address":"Cra. 21 #71-60","password":"123456","password_confirmation":"123456"}' http://localhost:8000/api/user

**Delete:**
curl -X DELETE -H 'Content-type: application/json' -H 'Accept: application.json' -d '{"id":"1"}' http://localhost:8000/api/user


- **Informacion adicional**

**List:**
curl -H 'Accept: application/json' http://localhost:8000/api/additionals

**Create:**
curl -X POST -H 'Content-type: application/json' -H 'Accept: application/json' -d '{"additional":"Cine"}' http://localhost:8000/api/additional

**Read:**
curl -H 'Accept: application/json' http://localhost:8000/api/additional/1

**Update:**
curl -X PUT -H 'Content-type: application/json' 'Accept: application/json' -d '{"additional":"Baile"}' http://localhost:8000/api/additional/1

**Delete:**
curl -X DELETE 'Accept: application.json' -d '{"id":"1"}' http://localhost:8000/api/additional/1

