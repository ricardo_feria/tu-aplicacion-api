<?php

namespace Api\Users\Controllers;

use App\Http\Controllers\Controller;

use Api\Users\Repositories\UserRepository;
use Api\Users\Requests\CreateUserRequest;
use Api\Users\Requests\ReadUserRequest;
use Api\Users\Requests\UpdateUserRequest;
use Api\Users\Requests\DeleteUserRequest;
use Api\Users\Resources\UserResource;

class UserController extends Controller
{
    /**
     * User repository implementation.
     * 
     * @var Api\Users\Repositories\UserRepository
     */
    private $users;

    /**
     * Returns an user's controller instance
     * 
     * @param Api\Users\Repositories\UserRepository  $userRepository
     * @return Api\Users\Controllers\UserController
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->users = $userRepository;
    }

    /**
     * Returns a list of all users
     * 
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        if ($users = $this->users->all()) {
            return UserResource::collection($users);
        }
        return response()->json([
            'data' => [
                'error' => '',
                'statusCode' => '500',
            ],
        ],500);
    }

    /**
     * Creates a new user in storage
     * 
     * @param Api\Users\Requests\CreateUserRequest  $request
     * @return Illuminate\Http\Response
     */
    public function create(CreateUserRequest $request)
    {
        if ($user = $this->users->create($request->all())) {
            return new UserResource($user);
        }
        return response()->json([
            'data' => [
                'error' => 'User not created',
                'statusCode' => '500',
            ],
        ], 500);
    }

    /**
     * Retrieves an specified user from storage
     * 
     * @param Api\Users\Requests\ReadUserRequest  $request
     * @return Illuminate\Http\Response
     */
    public function read(ReadUserRequest $request)
    {
        if ($user = $this->users->find($request->get('id'))) {
            return new UserResource($user);
        }
        return response()->json([
            'data' => [
                'error' => 'User not found',
                'statusCode' => '404',
            ],
        ], 404);
    }

    /**
     * Updates a specified user in storage
     * 
     * @param Api\Users\Requests\UpdateUserRequest  $request
     * @return Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request)
    {
        if ($user = $this->users->update($request->all())) {
            return new UserResource($user);
        }
        return response()->json([
            'data' => [
                'error' => 'User not updated',
                'statusCode' => '500',
            ],
        ], 500);
    }

    /**
     * Deletes a specified user in storage
     * 
     * @param Api\Users\Requests\DeleteUserRequest  $request
     * @return Illuminate\Http\Response
     */
    public function delete(DeleteUserRequest $request)
    {
        if ($this->users->delete($request->get('id'))) {
            return response()->json([
                'data' => [
                    'response' => 'User deleted',
                    'statusCode' => '200',
                ],
            ], 200);
        }
        return response()->json([
            'data' => [
                'error' => 'User not deleted',
                'statusCode' => '500',
            ],
        ], 500);
    }
}