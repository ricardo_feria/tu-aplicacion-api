<?php

namespace Api\Users\Repositories;

use Illuminate\Support\Facades\Hash;

use Api\Users\Models\User;

class UserRepository
{
    /**
     * Retrieves all users from the database
     * 
     * @return Api\Users\Models\User 
     */
    public function all()
    {
        return User::paginate(10);
    }

    /**
     * Creates a new user in the database
     * 
     * @param array
     * @return Api\Users\Models\User
     */
    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'address' => $data['address'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Retrieves a specified user from the database
     * 
     * @param int
     * @return Api\Users\Models\User
     */
    public function find(int $id)
    {
        return User::find($id);
    }

    /**
     * Updates a specified user in the database
     * 
     * @param array
     * @return Api\Users\Models\User
     */
    public function update(array $data)
    {
        $user = $this->find($data['id']);

        $user->name = $data['name'];
        $user->lastname = $data['lastname'];
        $user->phone = $data['phone'];
        $user->email = $data['email'];
        $user->address = $data['address'];
        if (isset($data['password'])) {
            $user->password = Hash::make($data['password']);
        }

        return $user->save() ? $user : NULL;
    }

    /**
     * Deletes a specified user in the database
     * 
     * @param int
     * @return boolean
     */
    public function delete(int $id)
    {
        return $this->find($id)->delete();
    }
}