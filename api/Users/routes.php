<?php

Route::namespace('Api\Users\Controllers')
    ->prefix('api')
    ->middleware('api')
    ->group(function() {
        Route::get('users', 'UserController@index');
        Route::post('user', 'UserController@create');
        Route::get('user', 'UserController@read');
        Route::put('user', 'UserController@update');
        Route::delete('user', 'UserController@delete');
});