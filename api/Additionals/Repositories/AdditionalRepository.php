<?php

namespace Api\Additionals\Repositories;

use Api\Additionals\Models\Additional;

class AdditionalRepository
{
    /**
     * Retrieves all additionals from the database
     * 
     * @return Api\Additionals\Models\Additional 
     */
    public function all()
    {
        return Additional::paginate(10);
    }

    /**
     * Creates a new additional in the database
     * 
     * @param array
     * @return Api\Additionals\Models\Additional
     */
    public function create(array $data)
    {
        return Additional::create([
            'name' => $data['additional'],
        ]);
    }

    /**
     * Retrieves a specified additional from the database
     * 
     * @param int
     * @return Api\Additionals\Models\Additional
     */
    public function find(int $id)
    {
        return Additional::find($id);
    }

    /**
     * Updates a specified additional in the database
     * 
     * @param array
     * @return Api\Additionals\Models\Additional
     */
    public function update(array $data, int $id)
    {
        $additional = $this->find($id);

        $additional->name = $data['additional'];

        return $additional->save() ? $additional : NULL;
    }

    /**
     * Deletes a specified additional in the database
     * 
     * @param int
     * @return boolean
     */
    public function delete(int $id)
    {
        return $this->find($id)->delete();
    }
}