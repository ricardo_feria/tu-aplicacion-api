<?php

namespace Api\Additionals\Controllers;

use App\Http\Controllers\Controller;

use Api\Additionals\Repositories\AdditionalRepository;
use Api\Additionals\Requests\AdditionalRequest;
use Api\Additionals\Resources\AdditionalResource;

class AdditionalController extends Controller
{
    /**
     * Additional repository implementation
     * 
     * @var Api\Additionals\Repositories\AdditionalRepository
     */
    private $additionals;

    /**
     * Returns an additional's controller instance
     * 
     * @param Api\Additionals\Repositories\AdditionalRepository  $additionalRepository
     * @return Api\Additionals\Controllers\AdditionalController
     */
    public function __construct(AdditionalRepository $additionalRepository)
    {
        $this->additionals = $additionalRepository;
    }

    /**
     * Retrieves a list of all additionals from storage
     * 
     * @return Illuminate\Http\Response  
     */
    public function index()
    {
        if ($additionals = $this->additionals->all()) {
            return AdditionalResource::collection($additionals);
        }
        return response()->json([
            'data' => [
                'error' => '',
                'statusCode' => '500',
            ],
        ],500); 
    }

    /**
     * Creates a new additional in storage
     * 
     * @param Api\Additionals\Requests\AdditionalRequest  $request
     * @return Illuminate\Http\Response
     */
    public function create(AdditionalRequest $request)
    {
        if ($additional = $this->additionals->create($request->all())) {
            return new AdditionalResource($additional);
        }
        return response()->json([
            'data' => [
                'error' => 'Additional not created',
                'statusCode' => '500',
            ],
        ], 500);
    }

    /**
     * Retrieves a specified additional from storage
     * 
     * @param int  $id
     * @return Illuminate\Http\Response
     */
    public function read(int $id)
    {
        if ($additional = $this->additionals->find($id)) {
            return new AdditionalResource($additional);
        }
        return response()->json([
            'data' => [
                'error' => 'Additional not found',
                'statusCode' => '404',
            ],
        ], 404);
    }

    /**
     * Updates a specified additional in storage
     * 
     * @param Api\Additionals\Requests\AdditionalRequest  $request
     * @param int  $id
     * @return Illuminate\Http\Response
     */
    public function update(AdditionalRequest $request, int $id)
    {
        if ($additional = $this->additionals->update($request->all(), $id)) {
            return new AdditionalResource($additional);
        }
        return response()->json([
            'data' => [
                'error' => 'Additional not updated',
                'statusCode' => '500',
            ],
        ], 500);
    }

    /**
     * Deletes a specified additional in storage
     * 
     * @param int  $id
     * @return Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        if ($this->additionals->delete($id)) {
            return response()->json([
                'data' => [
                    'response' => 'Additional deleted',
                    'statusCode' => '200',
                ],
            ], 200);
        }
        return response()->json([
            'data' => [
                'error' => 'Additional not deleted',
                'statusCode' => '500',
            ],
        ], 500);
    }
}