<?php

Route::namespace('Api\Additionals\Controllers')
    ->prefix('api')
    ->middleware('api')
    ->group(function() {
        Route::get('additionals', 'AdditionalController@index');
        Route::post('additional', 'AdditionalController@create');
        Route::get('additional/{id}', 'AdditionalController@read')->where('id', '[0-9]+');
        Route::put('additional/{id}', 'AdditionalController@update')->where('id', '[0-9]+');
        Route::delete('additional/{id}', 'AdditionalController@delete')->where('id', '[0-9]+');
});