<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMyRoutes();
        $this->loadMyMigrations();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Loads application modules routes
     * 
     * @return void
     */
    public function loadMyRoutes()
    {
        $modules = scandir(base_path('api'));
        foreach ($modules as $module)
        {
            if (file_exists($routes = base_path('api/'.$module.'/routes.php')))
            {
                $this->loadRoutesFrom($routes);
            }
        }
    }

    /**
     * Loads application modules migrations
     * 
     * @return void
     */
    public function loadMyMigrations()
    {
        $modules = scandir(base_path('api'));
        foreach ($modules as $module)
        {
            $migrations = base_path('api/'.$module.'/Database/Migrations');
            if (file_exists($migrations) && is_dir($migrations))
            {
                $this->loadMigrationsFrom($migrations);
            }
        }
    }
}
